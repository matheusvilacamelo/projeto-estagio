package com.vilaca;

import com.vilaca.helper.DecodificadorStatus;
import com.vilaca.helper.ManipuladorArquivo;
import com.vilaca.model.ListaDeTarefas;
import com.vilaca.model.Tarefa;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Main {

    private static ListaDeTarefas listaDeTarefas;
    private static Scanner scanner;

    public static void main(String[] args){
        System.out.println();
        System.out.println("Task in task v1.0");

        listaDeTarefas = new ListaDeTarefas();
        scanner = new Scanner(System.in);
        menuPrincipal();
    }

    public static void menuPrincipal(){
        System.out.println();
        System.out.println("Suas tarefas:");
        System.out.println();
        listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_DATA_CRIACAO, false);
        System.out.println();
        System.out.println("Para criar uma nova tarefa, insira 'n'.");
        System.out.println("Para acessar uma tarefa já existente, insira o número referente a tarefa.");
        System.out.println("Para filtrar a lista de tarefas por data de criação, insira 'c'.");
        System.out.println("Para filtrar a lista de tarefas por prioridade, insira 'p'.");
        System.out.println("Para filtrar a lista de tarefas por prazo, insira 'd'.");
        System.out.println("Para exibir a lista de tarefas feitas e fechadas, insira 'f'.");

        while (true){
            String entrada = scanner.next();

            if(entrada.matches(".*\\d.*")){
                menuTarefa(Integer.parseInt(entrada) - 1); //menos 1, pois a lista começa em 0 e os ids em 1
            }else{

                switch(entrada.charAt(0)) {
                    case 'n':
                        menuCriarNovaTarefa();
                        break;

                    case 'c':
                        listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_DATA_CRIACAO, false);
                        break;

                    case 'p':
                        listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_PRIORIDADE, false);
                        break;

                    case 'd':
                        listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_DATA_LIMITE, false);
                        break;

                    case 'f':
                        menuTarefasFechadasEConcluidas();
                        break;

                    default:
                        System.out.println("Comando não reconhecido, tente novamente.");
                }
            }
        }
    }

    public static void menuTarefa(int idTarefa){

        Tarefa tarefa;

        try {

           tarefa = listaDeTarefas.getTarefas().get(idTarefa);

        }catch (Exception e){
            System.out.println("Não foi possível encontrar uma tarefa com esse identificador, tente novamente.");
            return;
        }

        System.out.println();
        System.out.println("#" + tarefa.getId());
        System.out.println("Título: " + tarefa.getTitulo());
        System.out.println("Descrição: " + tarefa.getDescricao());
        System.out.println("Prioridade: " + tarefa.getPrioridade());
        System.out.println("Prazo: " + tarefa.getDataLimite() + " às " + tarefa.getHorarioLimite());
        System.out.println("Tempo estimado: " + tarefa.getTempoEstimado() + " hora(s)");
        System.out.println("Tempo fazendo a tarefa: " + tarefa.verificarTempoGasto());
        System.out.println("Status: " + DecodificadorStatus.decodificarStatus(tarefa.getStatus()));
        System.out.println();
        System.out.println("Para alterar o status da tarefa para 'fazendo', insira 'w'.");
        System.out.println("Para alterar o status da tarefa para 'feita', insira 'd'.");
        System.out.println("Para remover a tarefa, insira 'r'.");
        System.out.println("Para voltar para o menu principal, insira 'm'.");

        while (true){
            String entrada = scanner.next();

            switch(entrada.charAt(0)) {
                case 'm':
                    menuPrincipal();
                    break;

                case 'w':
                    if (listaDeTarefas.alterarStatusTarefa(idTarefa, Tarefa.FAZENDO)){
                        System.out.println("Status da tarefa alterado");
                    }else System.out.println("Não foi possível alterar o status da tarefa, tente novamente.");
                    menuPrincipal();
                    break;

                case 'd':
                    if (listaDeTarefas.alterarStatusTarefa(idTarefa, Tarefa.FEITA)){
                        System.out.println("Status da tarefa alterado");
                    }else System.out.println("Não foi possível alterar o status da tarefa, tente novamente.");
                    menuPrincipal();
                    break;

                case 'r':
                    if (listaDeTarefas.removerTarefa(idTarefa)){
                        System.out.println("Tarefa removida com sucesso");
                    }else System.out.println("Não foi possível remover a tarefa, tente novamente.");
                    menuPrincipal();
                    break;

                default:
                    System.out.println("Comando não reconhecido, tente novamente.");
            }
        }
    }

    public static void menuCriarNovaTarefa(){
        System.out.println();
        System.out.println("Insira o título da tarefa");
        String titulo = scanner.nextLine();
        while (titulo.isEmpty()){
            System.out.println("Insira o título da tarefa");
            titulo = scanner.nextLine();
        }
        System.out.println("Insira uma breve descrição sobre a tarefa");
        String descricao = scanner.nextLine();
        System.out.println("Insira o valor da prioridade da tarefa");
        String prioridade = scanner.next();
        while (!prioridade.matches(".*\\d.*")){
            System.out.println("Por favor insira um número");
            prioridade = scanner.next();
        }
        System.out.println("Insira a data limite para execução da tarefa");
        String dataLimite = null;
        while (true) {
            dataLimite = scanner.next();
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            try {
                format.setLenient(false);
                format.parse(dataLimite);
                break;
            } catch (Exception e) {
                System.out.println("Por favor insira uma data no formato dd/mm/aaaa");
            }
        }
        System.out.println("Insira o horário limite para execução da tarefa");
        String horarioLimite = null;
        while (true) {
            horarioLimite = scanner.next();
            SimpleDateFormat format = new SimpleDateFormat("HH:mm");
            try {
                format.setLenient(false);
                format.parse(horarioLimite);
                break;
            } catch (Exception e) {
                System.out.println("Por favor insira um horário no formato hh:mm");
            }
        }
        System.out.println("Insira o tempo estimado para a execução da tarefa em horas");
        String tempoEstimado = scanner.next();
        while (!tempoEstimado.matches(".*\\d.*")){
            System.out.println("Por favor insira um número");
            tempoEstimado = scanner.next();
        }

        if(listaDeTarefas.criarTarefa(titulo, descricao, Integer.valueOf(prioridade), dataLimite, horarioLimite, Integer.valueOf(tempoEstimado))){
            System.out.println("Tarefa criada com sucesso");
        }else System.out.println("Erro ao criar tarefa, tente novamente");

        menuPrincipal();
    }

    public static void menuTarefasFechadasEConcluidas(){
        System.out.println();
        System.out.println("Tarefas feitas e fechadas:");
        System.out.println();
        listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_DATA_CRIACAO, true);
        System.out.println();
        System.out.println("Para filtrar a lista de tarefas por data de criação, insira 'c'.");
        System.out.println("Para filtrar a lista de tarefas por prioridade, insira 'p'.");
        System.out.println("Para filtrar a lista de tarefas por prazo, insira 'd'.");
        System.out.println("Para voltar para o menu principal, insira 'm'.");

        while (true){
            String entrada = scanner.next();

            switch(entrada.charAt(0)) {
                case 'm':
                    menuPrincipal();
                    break;

                case 'c':
                    listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_DATA_CRIACAO, true);
                    break;

                case 'p':
                    listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_PRIORIDADE, true);
                    break;

                case 'd':
                    listaDeTarefas.exibirTarefas(ListaDeTarefas.FILTRO_DATA_LIMITE, true);
                    break;

                default:
                    System.out.println("Comando não reconhecido, tente novamente.");
            }
        }
    }

}
