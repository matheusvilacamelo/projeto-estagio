package com.vilaca.model;

import com.vilaca.helper.DecodificadorStatus;
import com.vilaca.helper.ManipuladorArquivo;
import com.vilaca.helper.comparators.ComparadorDataLimite;
import com.vilaca.helper.comparators.ComparadorPrioridade;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

public class ListaDeTarefas {

    public static final int FILTRO_DATA_CRIACAO = 1;
    public static final int FILTRO_PRIORIDADE = 2;
    public static final int FILTRO_DATA_LIMITE = 3;

    private List<Tarefa> tarefas;

    public ListaDeTarefas(){
        tarefas = new ArrayList<>();
        carregarTarefas();
    }

    public List<Tarefa> getTarefas() {
        return tarefas;
    }

    public void carregarTarefas(){
        tarefas = ManipuladorArquivo.carregar();
    }

    public Boolean salvarTarefas(){
        return ManipuladorArquivo.salvar(tarefas);
    }

    public Boolean criarTarefa(String titulo, String descricao, int prioridade, String dataLimite, String horarioLimite, int tempoEstimado){
        Tarefa tarefa = new Tarefa(tarefas.size() + 1);
        tarefa.setTitulo(titulo);
        tarefa.setDescricao(descricao);
        tarefa.setPrioridade(prioridade);
        tarefa.setDataLimite(dataLimite);
        tarefa.setHorarioLimite(horarioLimite);
        tarefa.setTempoEstimado(tempoEstimado);

        this.tarefas.add(tarefa);
        return salvarTarefas();
    }

    public Boolean removerTarefa(int idTarefaRemovida){
        tarefas.get(idTarefaRemovida).setStatus(Tarefa.FECHADA);

        return salvarTarefas();
    }

    public Boolean alterarStatusTarefa(int idTarefa, int novoStatus){

        switch (novoStatus) {
            case Tarefa.FAZENDO:
                this.tarefas.get(idTarefa).setStatus(Tarefa.FAZENDO);
                this.tarefas.get(idTarefa).setTempoInicialFazendoTarefa(Calendar.getInstance().getTimeInMillis());
                break;

            case Tarefa.FEITA:
                this.tarefas.get(idTarefa).setStatus(Tarefa.FEITA);
                break;
        }
        return salvarTarefas();
    }

    public List<Tarefa> filtrarTarefas(int filtro){
        List<Tarefa> tarefasFiltradas = new ArrayList<>();
        tarefasFiltradas.addAll(this.tarefas);

        switch (filtro){
            case FILTRO_DATA_CRIACAO:
                // Já está filtrada por data de criação por padrão
                break;

            case FILTRO_PRIORIDADE:
                Collections.sort(tarefasFiltradas, new ComparadorPrioridade());
                break;

            case FILTRO_DATA_LIMITE:
                Collections.sort(tarefasFiltradas, new ComparadorDataLimite());
                break;

        }

        return tarefasFiltradas;
    }

    public void exibirTarefas(int filtro, boolean exibirFechadasEConcluidas){

        List<Tarefa> tarefasExibidas = filtrarTarefas(filtro);

            for(Tarefa tarefa : tarefasExibidas){

                if(tarefa.getStatus() > 2 && exibirFechadasEConcluidas){
                    System.out.println(tarefa.getTitulo() + "\t Prazo: " +
                            tarefa.getDataLimite() + "às " + tarefa.getHorarioLimite() +
                            "\t Prioridade: " + tarefa.getPrioridade() + "\t-> " + DecodificadorStatus.decodificarStatus(tarefa.getStatus()));
                }
                if(tarefa.getStatus() < 3 && !exibirFechadasEConcluidas){
                    System.out.println(tarefa.getId() + " - " + tarefa.getTitulo() + "\t Prazo: " +
                            tarefa.getDataLimite() + " às " + tarefa.getHorarioLimite() +
                            "\t Prioridade: " + tarefa.getPrioridade() + "\t-> " + DecodificadorStatus.decodificarStatus(tarefa.getStatus()));
                }

            }
    }

}
