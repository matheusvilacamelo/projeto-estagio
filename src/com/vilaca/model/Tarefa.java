package com.vilaca.model;

import java.util.Calendar;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

public class Tarefa {

    public static final int AFAZER = 1;
    public static final int FAZENDO = 2;
    public static final int FECHADA = 3;
    public static final int FEITA = 4;

    private int id;
    private String titulo;
    private String descricao;
    private int prioridade;
    private String dataLimite;
    private String horarioLimite;
    private int tempoEstimado;
    private long tempoInicialFazendoTarefa = 0;
    private int status = 1;

    public Tarefa(int id) {
        this.id = id;
    }

    public String verificarTempoGasto(){
        if(this.status == Tarefa.FAZENDO){
            long diferencaEmMs = Math.abs(Calendar.getInstance().getTimeInMillis() - tempoInicialFazendoTarefa);
            long diferencaEmHoras = TimeUnit.HOURS.convert(diferencaEmMs, TimeUnit.MILLISECONDS);

            return diferencaEmHoras + " hora(s)";
        }else return "Tarefa ainda não iniciada";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getPrioridade() {
        return prioridade;
    }

    public void setPrioridade(int prioridade) {
        this.prioridade = prioridade;
    }

    public String getDataLimite() {
        return dataLimite;
    }

    public void setDataLimite(String dataLimite) {
        this.dataLimite = dataLimite;
    }

    public String getHorarioLimite() {
        return horarioLimite;
    }

    public void setHorarioLimite(String horarioLimite) {
        this.horarioLimite = horarioLimite;
    }

    public int getTempoEstimado() {
        return tempoEstimado;
    }

    public void setTempoEstimado(int tempoEstimado) {
        this.tempoEstimado = tempoEstimado;
    }

    public long getTempoInicialFazendoTarefa() {
        return tempoInicialFazendoTarefa;
    }

    public void setTempoInicialFazendoTarefa(long tempoInicialFazendoTarefa) {
        this.tempoInicialFazendoTarefa = tempoInicialFazendoTarefa;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
