package com.vilaca.helper;

import com.vilaca.model.Tarefa;

public class DecodificadorStatus {

    public static String decodificarStatus(int intStatus){

        switch (intStatus) {
            case Tarefa.AFAZER:
                return "À fazer";

            case Tarefa.FAZENDO:
                return "Fazendo";

            case Tarefa.FECHADA:
                return "Fechada";

            case Tarefa.FEITA:
                return "Feita";
        }

        return null;

    }

}
