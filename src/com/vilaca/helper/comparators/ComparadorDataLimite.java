package com.vilaca.helper.comparators;

import com.vilaca.model.Tarefa;

import java.time.LocalDateTime;
import java.util.Comparator;

public class ComparadorDataLimite implements Comparator<Tarefa> {


    @Override
    public int compare(Tarefa o1, Tarefa o2) {
        String[] dataArray1 = o1.getDataLimite().split("/");
        String[] horarioArray1 = o1.getHorarioLimite().split(":");
        String[] dataArray2 = o2.getDataLimite().split("/");
        String[] horarioArray2 = o2.getHorarioLimite().split(":");

        //int year, int month, int dayOfMonth, int hour, int minute, int second, int nanoOfSecond
        LocalDateTime data1 = LocalDateTime.of(Integer.parseInt(dataArray1[2]), Integer.parseInt(dataArray1[1]), Integer.parseInt(dataArray1[0]), Integer.parseInt(horarioArray1[0]), Integer.parseInt(horarioArray1[1]));
        LocalDateTime data2 = LocalDateTime.of(Integer.parseInt(dataArray2[2]), Integer.parseInt(dataArray2[1]), Integer.parseInt(dataArray2[0]), Integer.parseInt(horarioArray2[0]), Integer.parseInt(horarioArray2[1]));

        if(data1.isBefore(data2)){
            return -1;
        }
        else if(data1.isAfter(data2)){
            return 1;
        }
        else{
            if(o1.getPrioridade() > o2.getPrioridade()){
                return -1;
            }
            else if(o1.getPrioridade() < o2.getPrioridade()){
                return 1;
            }
            else return 0;
        }
    }
}
