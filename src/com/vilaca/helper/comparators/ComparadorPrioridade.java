package com.vilaca.helper.comparators;

import com.vilaca.model.Tarefa;

import java.util.Comparator;

public class ComparadorPrioridade implements Comparator<Tarefa> {

    @Override
    public int compare(Tarefa o1, Tarefa o2) {
        if(o1.getPrioridade() < o2.getPrioridade()){
            return 1;
        }
        else if(o1.getPrioridade() > o2.getPrioridade()){
            return -1;
        }
        else return 0;
    }
}
