package com.vilaca.helper;

import com.vilaca.model.Tarefa;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ManipuladorArquivo {

    public static Boolean salvar(List<Tarefa> tarefas){
        try {
            BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("Tarefas.txt"));

            for(Tarefa tarefa : tarefas){
                bufferedWriter.write(Integer.toString(tarefa.getId()));
                bufferedWriter.newLine();
                bufferedWriter.write(tarefa.getTitulo());
                bufferedWriter.newLine();
                bufferedWriter.write(tarefa.getDescricao());
                bufferedWriter.newLine();
                bufferedWriter.write(Integer.toString(tarefa.getPrioridade()));
                bufferedWriter.newLine();
                bufferedWriter.write(tarefa.getDataLimite());
                bufferedWriter.newLine();
                bufferedWriter.write(tarefa.getHorarioLimite());
                bufferedWriter.newLine();
                bufferedWriter.write(Integer.toString(tarefa.getTempoEstimado()));
                bufferedWriter.newLine();
                bufferedWriter.write(Long.toString(tarefa.getTempoInicialFazendoTarefa()));
                bufferedWriter.newLine();
                bufferedWriter.write(Integer.toString(tarefa.getStatus()));
                bufferedWriter.newLine();
            }
            bufferedWriter.close();
            return true;
        }catch (Exception e){
            System.out.println("Erro ao salvar tarefas");
            return false;
        }
    }

    public static List<Tarefa> carregar(){
        try {

            File file = new File("Tarefas.txt");
            if (!file.exists()){
                file.createNewFile();
            }

            BufferedReader bufferedReader = new BufferedReader(new FileReader("Tarefas.txt"));
            List <Tarefa> tarefas = new ArrayList<>();
            String linha = "";

            while (bufferedReader.ready()) {
                Tarefa tarefa = new Tarefa(0);

                linha = bufferedReader.readLine();
                tarefa.setId(Integer.parseInt(linha));
                linha = bufferedReader.readLine();
                tarefa.setTitulo(linha);
                linha = bufferedReader.readLine();
                tarefa.setDescricao(linha);
                linha = bufferedReader.readLine();
                tarefa.setPrioridade(Integer.parseInt(linha));
                linha = bufferedReader.readLine();
                tarefa.setDataLimite(linha);
                linha = bufferedReader.readLine();
                tarefa.setHorarioLimite(linha);
                linha = bufferedReader.readLine();
                tarefa.setTempoEstimado(Integer.parseInt(linha));
                linha = bufferedReader.readLine();
                tarefa.setTempoInicialFazendoTarefa(Long.parseLong(linha));
                linha = bufferedReader.readLine();
                tarefa.setStatus(Integer.parseInt(linha));

                tarefas.add(tarefa);
            }
            bufferedReader.close();
            return tarefas;
        }catch (Exception e){
            System.out.println("Erro ao carregar tarefas");
        }
        return null;
    }

}
